JIRA Subversion Plugin
=======================
JIRA's Subversion integration lets you see Subversion commit information relevant to each JIRA issue.

Documentation
-------------
https://ecosystem.atlassian.net/wiki/display/SVN/JIRA+Subversion+plugin

Marketplace
-----------
Binary version of this plugin are available on Marketplace:
https://marketplace.atlassian.com/plugins/com.atlassian.jira.plugin.ext.subversion/server/overview

Support
-----------
Plugin is unsupported by Atlassian
